/*
  @desc model for subjectsFact table
  @author Chatenet Ian
*/
const { con } = require("../helpers/databaseCo.js");
const { useDB } = require("../helpers/useDatabase.js");

class subjectsFactModel {
  static create(subjects,factId, cb) {
    useDB(con);
    let sql = `INSERT INTO subjectsfact(factID,subjectID) VALUE ?`;
    let values=[]

    subjects.forEach((subject) => {
      let row=[factId,subject.id]
      values.push(row)
    });
console.log(values)
    con.query(sql,[values], function (err, result) {
      if (err) throw err;
      cb(result);
    });
  }
}

module.exports = {
  subjectsFactModel,
};
