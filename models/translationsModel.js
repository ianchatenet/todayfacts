/*
  @desc model for translations table
  @author Chatenet Ian
*/
const { con } = require("../helpers/databaseCo.js");
const { useDB } = require("../helpers/useDatabase.js");

class translationsModel {

static create(body,cb){

    let sql = `INSERT INTO translations (frenchTitle,englishTitle,frenchContent,englishContent)VALUES
    ("${body.frenchTitle}","${body.englishTitle}","${body.frenchContent}","${body.englishContent}")`;
    useDB(con);

    con.query(sql, function (err, result) {
      if (err) throw err;
      cb(result)
    });
}

}

module.exports = {
  translationsModel,
};
