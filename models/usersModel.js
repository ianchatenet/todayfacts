/*
  @desc model for users table
  @author Chatenet Ian
*/
const { con } = require("../helpers/databaseCo.js");
const { useDB } = require("../helpers/useDatabase.js");

const usersModel = {
  /*
      @desc find one user in the database
      @author Chatenet Ian
    */
  findOne(body, cb) {
    let sql = `SELECT * FROM users WHERE email="${body.email}"`;
    useDB(con);

    con.query(sql, function (err, result) {
      if (err) throw err;
      cb(result);
    });
  },

  /*
    @desc find All users in the database
    @author Chatenet Ian
  */
  findAll() {
    let sql = `SELECT * FROM users`;

    con.connect(function (err) {
      if (err) throw err;
      console.log("Connected!");

      useDB(con);

      con.query(sql, function (err, result) {
        if (err) throw err;
        console.log(result);
      });
    });
  },
};

module.exports = {
  usersModel,
};
