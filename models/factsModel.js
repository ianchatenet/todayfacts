/*
  @desc model for facts table
  @author Chatenet Ian
*/
const { con } = require("../helpers/databaseCo.js");
const { useDB } = require("../helpers/useDatabase.js");

class factsModel {

  static findOne(body, cb) {
    let sql = `SELECT frenchTitle,englishTitle,frenchContent,englishContent,GROUP_CONCAT(DISTINCT subjects.name SEPARATOR ',')AS subjects FROM subjectsfact 
    LEFT JOIN facts ON subjectsfact.factID 
    LEFT JOIN subjects ON subjectsfact.subjectID 
    LEFT JOIN translations ON facts.translationID 
    WHERE today=true 
    GROUP BY subjectsfact.id`;
    useDB(con);

    con.query(sql, function (err, result) {
      if (err) throw err;
      cb(result[0]);
    });
  }

static create(body,id,cb){

    let sql = `INSERT INTO facts (userID,today,used,translationID)VALUES
    ("${body.user.id}",false,false,"${id}")`;
    useDB(con);

    con.query(sql, function (err, result) {
      if (err) throw err;
      cb(result)
    });
}

}

module.exports = {
  factsModel,
};
