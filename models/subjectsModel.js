/*
  @desc model for subjects table
  @author Chatenet Ian
*/
const { con } = require("../helpers/databaseCo.js");
const { useDB } = require("../helpers/useDatabase.js");

class subjectsModel {
  static find(body, cb) {
    useDB(con);
    let sql = `SELECT * FROM subjects
      WHERE name = "${body.subjects[0]}"`;

      body.subjects.forEach((subject) => {
        sql = sql+` OR name = "${subject}"`
      });

      con.query(sql, function (err, result) {
        if (err) throw err;
        cb(result);
      });
  }
}

module.exports = {
  subjectsModel,
};
