/*
  @desc used to create facts fixtures
  @author Chatenet Ian
*/

const { con } = require("../helpers/databaseCo.js");
const { useDB } = require("../helpers/useDatabase.js");

let contentF="Elle a marqué les esprits dès l'Antiquité par son caractère sévère avec un"
let contentA="It marked the spirits since Antiquity by its severe character with a model of educational"


let sqlTransl= `INSERT INTO translations(frenchTitle,englishTitle,frenchContent,englishContent)VALUES("Sparte une cité original","Sparta an original city","${contentF}","${contentA}")`
let sqlSubjects=`INSERT INTO subjects(name)VALUES("Antiquité")`
let sqlSubjects2=`INSERT INTO subjects(name)VALUES("Religion")`
let sqlFacts = `INSERT INTO facts (userID, today, used, translationID) VALUES (1,true,false, 1)`;
let sqlSubjectFacts = `INSERT INTO subjectsfact (factID,subjectID)VALUES(1,1)`
let sqlSubjectFacts2 = `INSERT INTO subjectsfact (factID,subjectID)VALUES(1,2)`
con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");

  useDB(con);


  con.query(sqlTransl, function (err, result) {
    if (err) throw err;
    console.log("Result: " + result);
  });
  con.query(sqlSubjects, function (err, result) {
    if (err) throw err;
    console.log("Result: " + result);
  });

  con.query(sqlSubjects2, function (err, result) {
    if (err) throw err;
    console.log("Result: " + result);
  });

  con.query(sqlFacts, function (err, result) {
    if (err) throw err;
    console.log("Result: " + result);
  });
  con.query(sqlSubjectFacts, function (err, result) {
    if (err) throw err;
    console.log("Result: " + result);
  });

  con.query(sqlSubjectFacts2, function (err, result) {
    if (err) throw err;
    console.log("Result: " + result);
  });
});
