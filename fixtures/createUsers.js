/*
  @desc this file is used to create fixtures here 
  @author Chatenet Ian
*/

const { con } = require("../helpers/databaseCo.js");
const { useDB } = require("../helpers/useDatabase.js");
const { passEncrypt } = require("../helpers/passEncrypt.js");


let pass = passEncrypt.encrypt("pass");

let sql = `INSERT INTO users (lastName, firstName, email, role, password, createdAt) VALUES ('Chatenet', 'Ian','ian@chatenet.com', 'ADMIN', "${pass}", now())`;

con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");

  useDB(con);

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Result: " + result);
  });
});
