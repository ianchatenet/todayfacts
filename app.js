/*
  @desc this file is the config of the app server all routes are here
  @author Chatenet Ian
*/
const express = require("express");
const bodyParser = require("body-parser");
const cors = require('cors')
require('dotenv').config()

const app = express();

//middleware 

app.use(bodyParser.urlencoded({ extended: false })); //parse request's body
app.use(bodyParser.json());
app.use(cors())

//@route home
const fact = require("./routes/facts.js");
app.use("/api", fact)


//@route admin
const admin = require("./routes/admin.js");
app.use("/api/admin", admin)


const port = (process.env.PORT || 3000);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
