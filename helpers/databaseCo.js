/*
  @desc this file is used in every database action to connect to the database
  @author Chatenet Ian
*/

require('dotenv').config()

var mysql = require('mysql');


var con = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS
});

module.exports = {
    con
  };