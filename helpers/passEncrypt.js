/*
  @desc helpers used to encrypt and decrypt things
  @author Chatenet Ian
*/

const bcrypt = require("bcrypt");
const saltRounds = 10;

class passEncrypt {
  /*
      @desc method used to encrypt mostly password
      @author Chatenet Ian
    */
  static encrypt(password) {
    const salt = bcrypt.genSaltSync(saltRounds);
    const hash = bcrypt.hashSync(password, salt);
    return hash
  }

  /*
    @desc method used to decrypt mostly password
    @author Chatenet Ian
  */
  static passDecrypt(reqPass, dbPass) {
    let match = bcrypt.compareSync(reqPass, dbPass);
    return match
  }
}

module.exports = {
  passEncrypt,
};
