function useDB(con) {
  
      con.query(`use ${process.env.DB_NAME}`, function (err, result) {
        if (err) throw err;
        console.log(`use ${process.env.DB_NAME} as ${process.env.DB_USER}`);
      });
    
  }

  module.exports = {
    useDB
  };