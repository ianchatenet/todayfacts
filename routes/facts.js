/*
  @desc home routes
  @author Chatenet Ian
*/
var express = require('express');
var router = express.Router();
const { factController: fact } = require("../controllers/factsController.js");

router.get('/fact', function(req, res) {
  fact.getFact(req,res);
});

router.post('/fact',function(req,res){
  fact.createFact(req,res)
})

module.exports = router;