/*
  @desc admin routes
  @author Chatenet Ian
*/
var express = require('express');
var router = express.Router();
const { adminController: admin } = require("../controllers/adminController.js");
const jwt = require("jsonwebtoken");

router.use("/api",function (req, res, next) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]
  console.log(req.headers)
  if(token == null) console.log("null")

  jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,user)=>{
    if(err) return res.sendStatus(403)
    req.user = user
    next()
  })
  next();
});

router.post('/login', function(req, res) {
  admin.login(req,res);
});

module.exports = router;