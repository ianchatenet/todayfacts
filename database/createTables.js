/*
  @desc this file is used to create all the tables of the database
  @author Chatenet Ian
*/

function createTables(con) {
  let sql = `CREATE TABLE`;

    tableUsers(sql,con);
    tableTransalations(sql,con);
    tableFacts(sql,con);
    tableSubjects(sql,con);
    tableSubjectsFact(sql,con);
}

function tableUsers(baseSQL,con) {
  let sql = `${baseSQL} users(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT ,
    lastName VARCHAR(20),
    firstName VARCHAR(20),
    email VARCHAR(50) UNIQUE,
    role VARCHAR(10),
    password VARCHAR(200),
    createdAT DATE
    )`;

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("table users created");
  });
}

function tableTransalations(baseSQL,con){
  let sql=`${baseSQL} translations(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    frenchTitle VARCHAR(200),
    englishTitle VARCHAR(200),
    frenchContent VARCHAR(500),
    englishContent VARCHAR(500)
  )`

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("table translations created ");
  });
}

function tableFacts(baseSQL,con){
  let sql=`${baseSQL} facts(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    userID INT ,
    today boolean,
    used boolean,
    translationID INT,
    FOREIGN KEY (translationID) REFERENCES translations(id),
    FOREIGN KEY (userID) REFERENCES users(id)
  )`

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("table facts created");
  });
}

function tableSubjects(baseSQL,con){
  let sql=`${baseSQL} subjects(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(20)
  )`

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("table subjects created");
  });
}

function tableSubjectsFact(baseSQL,con){
  let sql=`${baseSQL} subjectsFact(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    factID INT,
    subjectID INT,
    FOREIGN KEY (factID) REFERENCES facts(id),
    FOREIGN KEY (subjectID) REFERENCES subjects(id)
  )`

  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("table subjects fact created");
  });
}

module.exports = {
  createTables
};