/*
  @desc this file is used in the command createDB 
  @author Chatenet Ian
*/

const { con } = require("../helpers/databaseCo.js");
require('dotenv').config()

const { createDatabase } = require("./createDatabase.js");
const { createTables } = require("./createTables.js");

con.connect(function (err) {
  if (err) throw err;
  console.log("Connected!");

  createDatabase(con,process.env.DB_NAME)
  con.query(`use ${process.env.DB_NAME}`, function (err, result) {
    if (err) throw err;
    console.log(`use ${process.env.DB_NAME} as ${process.env.DB_USER}`);
  });

  createTables(con)
});


