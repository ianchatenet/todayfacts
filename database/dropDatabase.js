/*
  @desc this file is used in the command createDB 
  @author Chatenet Ian
*/

const { con } = require("../helpers/databaseCo.js");
require('dotenv').config()

let dbName = process.env.DB_NAME

con.query(`DROP database ${dbName}`, function (err, result) {
    if (err) throw err;
    console.log("Database dropped");
  });