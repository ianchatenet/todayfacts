/*
  @desc this file is used to create the database
  @author Chatenet Ian
*/

function createDatabase(con,dbName){
    con.query(`CREATE database ${dbName}`, function (err, result) {
        if (err) throw err;
        console.log("Database created");
      });
}

module.exports = {
    createDatabase
  };