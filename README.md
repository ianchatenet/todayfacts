# todayfacts

# technos

- node.js v 12.7.0
- MySQL v 8.0.22

# installation 

- create the .env at the project's root with variables:

    - DB_HOST = your database host
    - DB_USER = your database user 
    - DB_PASS = your password
    - DB_NAME = your database name
    - PORT  = the port you want to use

- in shell run:

    - npm install
    - npm run createDB
    - npm start

- go to http://localhost:8080





# npm command 

- npm install (install dependencies)
- npm start (start server)
- npm run createDB (create database with all tables)
- npm run dropDB (drop the database and all the data in it)
