/*
  @desc Home Controller
  @author Chatenet Ian
*/

const { factsModel } = require("../models/factsModel.js");
const { translationsModel } = require("../models/translationsModel.js");
const { subjectsModel } = require("../models/subjectsModel.js");
const { subjectsFactModel } = require("../models/subjectsFactModel.js");
const { con } = require("../helpers/databaseCo.js");



class factController {
  //@route /api/fact GET used to get fact on home page and display it
  static getFact(req, res) {
    factsModel.findOne(req.body, function (result) {
      let subjects = result.subjects.split(",");
      let fact = {
        subjects: subjects,
        title: {
          french: result.frenchTitle,
          english: result.englishTitle,
        },
        content: {
          french: result.frenchContent,
          english: result.englishContent,
        },
      };
      res.json({ fact });
    });
  }
//@route /api/fact POST
  static createFact(req,res){
    let factsId
    translationsModel.create(req.body,function(result){
      
      factsModel.create(req.body,result.insertId,function(result){
        factsId=result.insertId
        subjectsModel.find(req.body,function(result){
            subjectsFactModel.create(result,factsId,function(result){
              return res.sendStatus(200)
            })
          
        })

      })
    })
    
      
  }
}

module.exports = {
  factController,
};
