/*
  @desc Admin Controller
  @author Chatenet Ian
*/

const { usersModel } = require("../models/usersModel.js");
const { passEncrypt } = require("../helpers/passEncrypt.js");
const jwt = require("jsonwebtoken");
const adminController = {

  login(req, res) {
    usersModel.findOne(req.body, function (result) {
      if(result.length < 1)return res.status(401).send("Invalid credentials");
      
      let match = passEncrypt.passDecrypt(
        req.body.password,
        result[0].password
      );
      if (match === false) return res.status(401).send("Invalid credentials");
      const user = {
        lastName: result[0].lastName,
        firstName: result[0].firstName,
        email:result[0].email,
        id:result[0].id
      };
      const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET);
      res.json({ user: user, token: accessToken });
    });
  }
  
};

module.exports = {
  adminController,
};
