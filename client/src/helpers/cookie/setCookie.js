export default function setCookie(name, value, expiration) {
    var d = new Date();
    d.setTime(d.getTime() + (expiration*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
  }