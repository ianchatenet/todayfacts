import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./components/home/Home.jsx";
import Admin from "./components/admin/Admin.jsx";
import ErrorPage from "./components/ErrorPage.jsx";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact strict path="/admin">
          <Admin/>
        </Route>
        <Route exact strict path="/">
          <Home />
        </Route>
        <Route>
          <ErrorPage error={{ message: "Url not found", code: 404 }} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
