/*
  @desc Admin login used to access the backend of the app
  @author Chatenet Ian
*/
import React from "react";

export default function Login(props) {


  return (
    <div>
      <h1>Login</h1>
      <form onSubmit={props.handleSubmit}>
        <label>email</label>
        <input name="email"></input>
        <label>password</label>
        <input name="password"></input>
        <button type="submit">submit</button>
      </form>
    </div>
  );
}
