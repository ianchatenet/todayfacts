/*
  @desc Admin page accessible if user is connected
  @author Chatenet Ian
*/

import React, { useState, useEffect } from "react";
import Login from "./Login.jsx";
import api from "../../api/index.js";
import getCookie from "../../helpers/cookie/getCookie.js";
import setCookie from "../../helpers/cookie/setCookie.js";

export default function Admin(props) {
  const [user, setUser] = useState(false);
  const [log, setLog] = useState(false);

  useEffect(() => {
    let userStored = getCookie("user");
    if (!userStored) setUser(user);
    else setUser(JSON.parse(userStored));
  }, [log]);

  async function handleSubmit(e) {
    e.preventDefault();
    let email = e.target.email.value;
    let password = e.target.password.value;
    let resp = await api.login(email, password);
    setCookie("user",JSON.stringify(resp.user),30)
    setCookie("token",resp.token,30)
    setLog(!log);
  }
  return (
    <div>
      <h1>admin</h1>
      {user ? <p>{user.firstName}</p> : <Login handleSubmit={handleSubmit} />}
    </div>
  );
}
