/*
  @desc Error page take an error as an argument 
  @author Chatenet Ian
*/

import React from 'react'

export default function ErrorPage(props){
    return(
        <div>
            <h2>Error {props.error.code}</h2>
            <p>{props.error.message}</p>
        </div>
    )
}