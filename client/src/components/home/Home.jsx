/*
  @desc Home page used to display the fact of today
  @author Chatenet Ian
*/

import React from "react";
import "./Home.css";

let facts = {
  translation: {
    fr:
      "francais truc mucheaaaaaaaaa aaaaaaaaaaaaaaaaaa aaaaaaaaaaaaaaaaa aaaaaaaaaaaaaa",
    en: "english bitonio",
  },
  subjects: ["hist", "géo"],
};

var myArray = [
  "Red",
  "Green",
  "Yellow",
  "Blue",
  "Dark"
];

var randomClass = myArray[Math.floor(Math.random()*myArray.length)];

export default function Home() {
  return (
    <main data-theme={randomClass}>
      <h1 className={"title"}>TodayFacts</h1>
      <div className={"factContainer"}>
        <div className={"subjects"}>
          <h4>Subjects</h4>
          <p>
            {facts.subjects.map((subj,index) => (
              <span key={index}>{subj} </span>
            ))}
          </p>
        </div>

        <p className={"contents"}>{facts.translation.fr}</p>
      </div>
    </main>
  );
}
