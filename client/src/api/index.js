let baseUrl = "http://localhost:8080/api";

const api = {
  async login(email, password) {
    let data = {
      email: email,
      password: password,
    };
    let resp = await fetch(baseUrl + "/admin/login", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "post",
      body: JSON.stringify(data),
    });
    if (!resp.ok) return resp.status;
    return resp.json();
  },
};

export default api;
